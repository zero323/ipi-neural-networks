# Unsupervised learning: Hebbian and competitive learning
Maciej Szymkiewicz  


Reproducing this report requires following external libraries.


```r
library(gridExtra)
library(ggplot2)
library(devtools)
```

R source code for all exercises including this document is available on [Bitbucket](https://bitbucket.org/zero323/ipi-neural-networks) and can be installed using following commands.


```r
devtools::install_bitbucket("zero323/ipi-neural-networks/ex3-multilayer-perceptron")
devtools::install_bitbucket("zero323/ipi-neural-networks/ex4-hebbian-and-competitive-learning")
library(ipiNeuralNetworksEx3)
library(ipiNeuralNetworksEx4)
```

All experiments have been performed using [iris dataset](./data/iris.data).


```r
X <- as.matrix(read.csv('data/iris.data', header = FALSE)[, 1:4])
Y <- as.matrix(read.csv('data/iris.data', header = FALSE)[, 5])
```

Random number generator seed:


```r
set.seed(323)
```

### Exercise 1.
> Implement simple Hebbian rule to perform clustering of the
standard Iris Flower input data. Try with 3 clusters.

Implementation: [`hebian_update_weights`](./R/ipiNeuralNetworksEx4.R?at=master&fileviewer=file-view-default#ipiNeuralNetworksEx4.R-11)


```r
network <- create_network(4, c(3), list(linear_neuron))
report_helper(
    network, X, Y, hebian_update_weights, 1, 1:150,
    "Hebbian rule, eta = 0.01, nepoch = 1"
)
```

![](report_files/figure-html/unnamed-chunk-5-1.png)<!-- -->

### Exercise 2.
> Implement Sejnowski's covariance rule to perform clustering of
the standard Iris Flower input data. Compare results with the Exercise 1

Implementation: [`sejnowski_update_weights`](./R/ipiNeuralNetworksEx4.R?at=master&fileviewer=file-view-default#ipiNeuralNetworksEx4.R-49)


```r
network <- create_network(4, c(3), list(linear_neuron))
report_helper(
    network, X, Y, sejnowski_update_weights(network), 1, 1:150,
    "Sejnowski's rule, eta = 0.01, nepoch = 1"
)
```

<img src="report_files/figure-html/unnamed-chunk-6-1.png" style="display: block; margin: auto;" />

### Exercise 3.
> Derive Oja's learning rule (4.17) using (4.14) from the textbook

**Note** Since derivation has been already provided in the textbook following equations should be considered as the notes and not the solution.


$$
w_{m}(t + 1) = \frac{w_{m}(t) + \eta y(t)x_{m}(t)}{(\sum_{j}^{M} (w_{j}(t) + \eta yx_{j})^p)^{1/p}} = \\
\frac{w_{i}}{ \left(  \sum_{j} w_{j}^{p} \right)^{1/p}  } + \eta \left(
\frac{yx_{i}}{ \left(  \sum_{j} w_{j}^{p} \right)^{1/p}  } -  
\frac{w_{i} \sum_{j} y x_{j} w_{j}}{ \left(  \sum_{j} w_{j}^{p} \right)^{1 + 1/p}  } +
O \left( \eta^{2} \right)
\right) = \\
\frac{w_{m}}{(\sum_{j}^{M} w_{j}^{p})^{1/p}}  + \left( 
\frac{yx_{m}}{(\sum_{j}^{p} w_{j}^{p})^{1/p}} - 
\frac{w_{m} \sum_{j}^{M} yw_{j}w_{j}}{(\sum_{j}^{p} w_{j}^{p})^{1 + 1/p}}
\right) \eta + O(\eta^{2})
$$

Assuming weights normalization it can simplified

$$
w_{m}(t + 1) = w_{m}(t) + \eta y(t)(x_{m} - y(t)w_{m}(t))
$$

and after vectorization we obtain equation 4.17.

### Exercise 4.
> Implement Oja's rule to perform 3-class clustering of the standard Iris Flower input data. Attach comparative analysis of the results

Implementation: [`oja_update_weight`](./R/ipiNeuralNetworksEx4.R?at=master&fileviewer=file-view-default#ipiNeuralNetworksEx4.R-29)


```r
network <- create_network(4, c(3), list(linear_neuron))
report_helper(
    network, X, Y, oja_update_weights, 100, 1:150,
    "Oja's rule, eta = 0.01, nepoch = 100"
)
```

<img src="report_files/figure-html/unnamed-chunk-7-1.png" style="display: block; margin: auto;" />

### Exercise 5.
> Implement competitive network with three nodes to perform 3-
class clustering of the standard Iris Flower input data. Attach comparative
analysis of the results.

Implementation: [`competitive_update_weights`](./R/ipiNeuralNetworksEx4.R?at=master&fileviewer=file-view-default#ipiNeuralNetworksEx4.R-84)


```r
network <- create_network(4, c(3), list(linear_neuron))
report_helper(
    network, X, Y, competitive_update_weights, 10, 1:150,
    "Competitive network, eta = 0.01, nepoch = 10"
)
```

<img src="report_files/figure-html/unnamed-chunk-8-1.png" style="display: block; margin: auto;" />
