# Evolutionary algorithms
Maciej Szymkiewicz  

Using of any program language, write evolutionary strategies for fitting of function to experimental data set.
The format of function is a polynomial:

$y(x) = ax^{2} + bx + c $

Find parameters: a, b, c.
Try to implement strategies: (1+1), ($\mu + \lambda$), ($\mu, \lambda$) 

Find and describe differences in efficiency. Try to find, what is a reason of differences in algorithms
operating.

### Loading required libraries and setting random number generator seed.

Source code for all exercies is available on [Bitbucket](https://bitbucket.org/zero323/ipi-neural-networks/) and can be installed using following comands:


```r
devtools::install_bitbucket("zero323/ipi-neural-networks/ex6-evolutionary-algorithms")
library(ipiNeuralNetworksEx6)
```

Load required libraries and set seed.


```r
library(ggplot2)
set.seed(323)
```

### Loading dataset


```r
actual <- read.csv("data/input.csv")

X <- actual$x
Y <- actual$y

evaluation_function <- function(params) {
    e <- Y - (params[1] * X ** 2 + params[2] * X + params[3])
    -sqrt(mean(e ** 2))
}

model_fit <- lm(y ~ I(x^2) + x, data = actual)
prediction <- predict(model_fit, actual)

plot_actual_vs_predict(actual$y, prediction, actual$x, "Regression model")
```

<img src="report_files/figure-html/unnamed-chunk-3-1.png" style="display: block; margin: auto;" />

### Strategy 1 + 1


```r
result_one_plus_one <- one_plus_one(
    x = rnorm(3),
    evaluation_function = evaluation_function,
    max_generations = 1000
)

pred_one_plus_one <- report_predict(result_one_plus_one$params, X)
plot_actual_vs_predict(Y, pred_one_plus_one, X, "1 + 1")
```

<img src="report_files/figure-html/unnamed-chunk-4-1.png" style="display: block; margin: auto;" />

### Strategy $\mu + \lambda$

- $\mu = 10$
- $\lambda = 90$


```r
result_mu_plus_lambda <- mu_plus_lambda(
    x = matrix(rnorm(30), ncol = 3), sigma = rep(1, 3), lambda = 30,
    evaluation_function = evaluation_function,
    max_generations = 1000
)

pred_mu_plus_lambda <- report_predict(result_mu_plus_lambda$params, X)
plot_actual_vs_predict(Y, pred_mu_plus_lambda , X, expression(mu + lambda))
```

<img src="report_files/figure-html/unnamed-chunk-5-1.png" style="display: block; margin: auto;" />


### Strategy $\mu, \lambda$

- $\mu = 10$
- $\lambda = 30$


```r
result_mu_lambda_10_30 <- mu_lambda(
    x = matrix(rnorm(30), ncol = 3), sigma = rep(1, 3), lambda = 30,
    evaluation_function = evaluation_function,
    max_generations = 1000
)

pred_mu_lambda_10_30 <- report_predict(result_mu_lambda_10_30$params, X)
plot_actual_vs_predict(Y, pred_mu_lambda_10_30 , X, expression(mu ~ ", " ~ lambda))
```

<img src="report_files/figure-html/unnamed-chunk-6-1.png" style="display: block; margin: auto;" />

- $\mu = 30$
- $\lambda = 90$



```r
result_mu_lambda <- mu_lambda(
    x = matrix(rnorm(90), ncol = 3), sigma = rep(1, 3), lambda = 90,
    evaluation_function = evaluation_function,
    max_generations = 1000
)

pred_mu_lambda <- report_predict(result_mu_lambda$params, X)
plot_actual_vs_predict(Y, pred_mu_lambda , X, expression(mu ~ ", " ~ lambda))
```

<img src="report_files/figure-html/unnamed-chunk-7-1.png" style="display: block; margin: auto;" />


### Summary

- $1 + 1$ strategy keeps only a single specimen per iteration it can search limited neighbourhood and shows the slowest convergence but with high enough number of generations provides very good estimation of the parameters. 
- $\mu + \lambda$ strategy is able to search large neighbourhood each iteration and converge much faster than the $1 + 1$ giving similar final results
- $\mu, \lambda$ strategy shows similar convergence rate to the $\mu + \lambda$ strategy but requires larger $\mu$ and $\lambda$ values to obtain similar results. Because we choose new population only from specimens of the temporary single "bad" iteration can obliterate all previous gains. It also gives less diverge population and reduces size of the neighbourhood we can search.



```r
scores <- data.frame(
    score = c(
        result_one_plus_one$scores, result_mu_plus_lambda$scores,
        result_mu_lambda_10_30$scores, result_mu_lambda$scores
    ),
    strategy = c(
        rep("1 + 1", 1000), rep("mu + lambda", 1000),
        rep("mu, lambda (10, 30)", 1000), rep("mu, lambda", 1000)
    ),
    iteration = rep(1:1000, 4)
)

p <- ggplot(scores, aes(x = iteration, y = score, col = strategy)) + geom_line() + theme_bw()
p + ggtitle("Evolutionary algorithms - -RMSE per iteration")
```

<img src="report_files/figure-html/unnamed-chunk-8-1.png" style="display: block; margin: auto;" />
