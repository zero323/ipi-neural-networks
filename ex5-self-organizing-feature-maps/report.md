# Self-Organizing Feature Maps
Maciej Szymkiewicz  

R source code for all exercises including this document is available on [BitBucket](https://bitbucket.org/zero323/ipi-neural-networks/) and can be installed using following commands.


```r
devtools::install_bitbucket("zero323/ipi-neural-networks/ex3-multilayer-perceptron")
devtools::install_bitbucket("zero323/ipi-neural-networks/ex5-self-organizing-feature-maps")
```

Load libraries and set random number generator seed.


```r
set.seed(1323)
library("ipiNeuralNetworksEx3")
library("ipiNeuralNetworksEx5")
```

## Exercise 1. 
> Implement the two-inputs SOM with 1D output lattice and test
it on a square grid with random samples.


```r
X <- matrix(runif(500, -3, 3), ncol = 2)
network <- create_som(input_count = 2, neuron_counts = 5, lattice_nrows = 5)
model <- train_som(network = network, X = X, nepoch = 100)
```


```r
reportHelper(model, X)
```

![](report_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

## Exercise 2.
> Implement the two-inputs SOM with 2D output lattice and test
it on a square grid with random samples.


```r
X <- matrix(runif(500, -3, 3), ncol = 2)
network <- create_som(
    input_count = 2, neuron_counts = 9, lattice_nrows = 3, lattice_ncols = 3
)
model <- train_som(network = network, X = X, nepoch = 100, n = 1)
```


```r
reportHelper(model, X)
```

![](report_files/figure-html/unnamed-chunk-6-1.png)<!-- -->

## Exercise 3.
> Basing on the previous exercise, extend the number of inputs to
4 and modify the learning rate and the forgetting factor to be exponentially
decreasing. Perform cluster analysis on the standard Iris Flower dataset.
Try a 3-by-3 output lattice.


```r
X <- as.matrix(iris[, 1:4])
network <- create_som(
    input_count = 4, neuron_counts = 9, lattice_nrows = 3, lattice_ncols = 3,
    eta = decreasing_eta(0.1),
    mins = apply(X, 2, min),
    maxs = apply(X, 2, max)
)
model <- train_som(
    network = network, X = X, nepoch = 10, n = 1, lambda = decreasing_lambda(0.01)
)
```


```r
reportHelper(model, X)
```

![](report_files/figure-html/unnamed-chunk-8-1.png)<!-- -->

## Exercise 4.
> Basing on the previous exercise, use larger output lattices such
that their deformations occur frequently. Try to avoid these deformations
with different neighborhood sizes {1,2,3}.


```r
X <- as.matrix(iris[, 1:4])
network <- create_som(
    input_count = 4, neuron_counts = 36, lattice_nrows = 6, lattice_ncols = 6,
    eta = decreasing_eta(0.2),
    mins = apply(X, 2, min),
    maxs = apply(X, 2, max)
)
```

### Neighborhood size = 1


```r
model <- train_som(
    network = network, X = X, nepoch = 10, n = 1, lambda = decreasing_lambda(0.01)
)
reportHelper(model, X)
```

![](report_files/figure-html/unnamed-chunk-10-1.png)<!-- -->

### Neighborhood size = 2


```r
model <- train_som(
    network = network, X = X, nepoch = 10, n = 2, lambda = decreasing_lambda(0.01)
)
reportHelper(model, X)
```

![](report_files/figure-html/unnamed-chunk-11-1.png)<!-- -->

### Neighborhood size = 3


```r
model <- train_som(
    network = network, X = X, nepoch = 10, n = 3, lambda = decreasing_lambda(0.01)
)
reportHelper(model, X)
```

![](report_files/figure-html/unnamed-chunk-12-1.png)<!-- -->

## Exercise 5.
> Perform comparative analysis of the Gaussian neighborhood
function with the Mexican Hat function (implemented by any of the two formulae).


```r
d <- seq(-3, 3, 0.1)
y <- mexican_hat(d = d, sigma = 1, alpha = 0.9, beta = 1.2)
plot(y ~ d, type = 'l')
title("Mexican hat function, sigma = 1, alpha = 0.9, beta = 1.2")
```

![](report_files/figure-html/unnamed-chunk-13-1.png)<!-- -->


```r
X <- as.matrix(iris[, 1:4])
network <- create_som(
    input_count = 4, neuron_counts = 16, lattice_nrows = 4, lattice_ncols = 4,
    eta = decreasing_eta(0.1),
    mins = apply(X, 2, min),
    maxs = apply(X, 2, max)
)
model <- train_som(
    network = network, X = X, nepoch = 100,
    lambda = decreasing_lambda(0.01),
    distmod_function = mexican_hat_distmod()
)
reportHelper(model, X)
```

![](report_files/figure-html/unnamed-chunk-14-1.png)<!-- -->
