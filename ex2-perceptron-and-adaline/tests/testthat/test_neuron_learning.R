library(testthat)

expect_equal(
    neuron_model(
        learning_rule=perception_learning_rule,
        activation_function=sign,
        eta=function(...) 0.1
    )(
        x = matrix(rep(1, 9), nrow=3),
        d = rep(1, 3),
        niters = 100
    )$rmse[100],
    0,
    info = "Perceptron should be able to deal with a trival example"
)