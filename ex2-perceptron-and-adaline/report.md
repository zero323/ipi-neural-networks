# Perceptron & ADALINE: learning rules
Maciej Szymkiewicz  



All exercises have been solved using R language. Source code is available on [BitBucket](https://bitbucket.org/zero323/ipi-neural-networks/) and can be installed using following commands:


```r
devtools::install_bitbucket("zero323/ipi-neural-networks/ex2-perceptron-and-adaline")
```

```
## Skipping install of 'ipiNeuralNetworksEx2' from a bitbucket remote, the SHA1 (0aa3ec3b) has not changed since last install.
##   Use `force = TRUE` to force installation
```

```r
library(ipiNeuralNetworksEx2)
```

Perceptron and  ADALINE implementations are using generic neuron model implemented by a [`neuron_model`](./R/hw2.R?at=master&fileviewer=file-view-default#hw2.R-74) function.
                                                                                                        
                                                                                                
### Exercise 1.
> Implement a single-unit perceptron together with its learning
algorithm

Perceptron learning algorithm is implemented by a [`perception_learning_rule`](./R/hw2.R?at=master&fileviewer=file-view-default#hw2.R-11) function. 


```r
perceprton <- neuron_model(
    learning_rule=perception_learning_rule,
    activation_function=sign,
    # Set learning rate to 0.01
    eta=function(...) 0.01
)

perceprton_0.001 <- neuron_model(
    learning_rule=perception_learning_rule,
    activation_function=sign,
    # Set learning rate to 0.01
    eta=function(...) 0.001
)

perceprton_0.0001 <- neuron_model(
    learning_rule=perception_learning_rule,
    activation_function=sign,
    # Set learning rate to 0.01
    eta=function(...) 0.0001
)
```

### Exercise 2.
> Implement a single ADALINE unit together with its Delta learning algorithm

ADALINE learning algorithm is implemented by a [`delta_learning_rule`](./R/hw2.R?at=master&fileviewer=file-view-default#hw2.R-30) function.


```r
adaline_decrease_0.1_1 <- neuron_model(
    learning_rule=delta_learning_rule,
    activation_function=function(x) x,
    eta=time_decreasing_eta(0.1, 1)
)

adaline_const_0.0001 <- neuron_model(
    learning_rule=delta_learning_rule,
    activation_function=function(x) x,
    eta=constant_eta(0.0001)
)

adaline_const_0.001 <- neuron_model(
    learning_rule=delta_learning_rule,
    activation_function=function(x) x,
    eta=constant_eta(0.001)
)

adaline_const_0.01 <- neuron_model(
    learning_rule=delta_learning_rule,
    activation_function=function(x) x,
    eta=constant_eta(0.01)
)
```

### Exercise 3.
> Perform a training of the perceptron on the OR-type function approximation (linearly separable)

Perceptrons converge to the optimal solution as expected with convergence speed highly dependant on learning rate parameter. Weights vector is as expected orthogonal to the decision line.

![](report_files/figure-html/unnamed-chunk-5-1.png)<!-- -->

![](report_files/figure-html/unnamed-chunk-6-1.png)<!-- -->


### Exercise 4.
> Perform a training of the ADALINE on the OR-type function
approximation (linearly separable). Try 3 different learning rates. Is it worth
to implement exponentially decreasing learning rate in this case

ADALINE neurons converge faster compared to perceptrons, but RMSE is much higher. Learning speed is less dependant on eta parameter. Exponentially decreasing learning rate shows the best performance on a training data but requires fitting two parameters.

![](report_files/figure-html/unnamed-chunk-7-1.png)<!-- -->

### Exercise 5.

> Perform comparative tests of the perceptron and ADALINE on the XOR-type function approximation (not linearly separable).
> Stop criterion: after a reasonable number of epochs

ADALINE neuron converges with a speed similar to the one for the OR-type function. Perceptron as expected cannot converge and RSME fluctuates around 1.35,

![](report_files/figure-html/unnamed-chunk-8-1.png)<!-- -->



```
## Removing package from '/home/zero323/R/x86_64-pc-linux-gnu-library/3.3'
## (as 'lib' is unspecified)
```
