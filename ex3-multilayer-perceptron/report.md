# Multilayer Perceptron & Feed-forward Neural Networks: error back-propagation supervised learning
Maciej Szymkiewicz  

R source code for all exercises including this document is available on [BitBucket](https://bitbucket.org/zero323/ipi-neural-networks/) and can be installed using following commands:


```r
set.seed(323)
devtools::install_bitbucket("zero323/ipi-neural-networks/ex3-multilayer-perceptron")
library(ipiNeuralNetworksEx3)
library(ggplot2)
```


## Exercise 1.

> Implement a 2-layer perceptron network (2 neurons in the input layer + 1 output unit) together with its learning algorithm. Train the network on XOR problem.


```r
X <- matrix(c(
    0, 0,
    1, 0,
    0, 1,
    1, 1),
   ncol = 2, byrow = TRUE
)

Y <- matrix(c(0, 1, 1, 0))

sigmoid_neuron.0.1 <- set_eta(sigmoid_neuron, function(...) 0.1)

rmse.ex1 <- average_rmse(
    times = 10, X = X, Y = Y, input_count = 2, neuron_counts = c(2, 1),
    layers = list(sigmoid_neuron.0.1, sigmoid_neuron.0.1),
    nepoch = 3500, momentum = 0
)
```


```r
p <- ggplot(data.frame(rmse=rmse.ex1, iteration=1:3500), aes(x = iteration, y = rmse))
p <- p + theme_bw() + ggtitle("Average RMSE by iteration. Number of trials: 10")
p  + geom_line()
```

![](report_files/figure-html/unnamed-chunk-1-1.png)<!-- -->

## Exercise 2.

> Derive function forms of derivatives for 3 activation functions:
linear, unipolar logistic and hyperbolic tangent. Express the derivatives using output values $y^{l}_{i}$ rather than inputs or weights. This will speed up the subsequent computations.


- [linear](./R/hw3.R?at=master&fileviewer=file-view-default#hw3.R-47)
- [unipolar logistic](./R/hw3.R?at=master&fileviewer=file-view-default#hw3.R-15)
- [hyperbolic tangent](./R/hw3.R?at=master&fileviewer=file-view-default#hw3.R-31)


## Exercise 3.
> Implement 3-layer feedforward neural network with the backpropagation algorithm (two layers with hyperbolic tangent unit + output layer with linear activations). Train on Non-linear Dynamic Plant benchmark.

- [generic network generator](./R/hw3.R?at=master&fileviewer=file-view-default#hw3.R-99)
- [feedforward algorithm](./R/hw3.R?at=master&fileviewer=file-view-default#hw3.R-119)
- backpropagation algorithm: [compute_deltas](./R/hw3.R?at=master&fileviewer=file-view-default#hw3.R-137) and [update_weights](./R/hw3.R?at=master&fileviewer=file-view-default#hw3.R-159)


```r
X <- as.matrix(read.table("data/NDP.dat")[, 1:2])
Y <- as.matrix(read.table("data//NDP.dat")[, 3])

rmse.ex3 <- average_rmse(
    times = 10, X = X, Y = Y, input_count = 2, neuron_counts = c(5, 5, 1),
    layers = list(hyperbolic_tangent_neuron, hyperbolic_tangent_neuron, linear_neuron),
    nepoch = 300, momentum = 0
)
```

## Exercise 4.
> Implement exponential increasing of the learning rate, and perform comparative analysis of the improvement on the dataset from Ex. 3.

Note: I assumed decreasing learning rate. As expected exponentially increasing rate doesn't perform very vell.


```r
hyperbolic_tangent_neuron_dec <- set_eta(hyperbolic_tangent_neuron, decreasing_eta(0.1))
linear_neuron_dec <- set_eta(linear_neuron, decreasing_eta(0.1))

rmse.ex4 <- average_rmse(
    times = 10, X = X, Y = Y, input_count = 2, neuron_counts = c(5, 5, 1),
    layers = list(
        hyperbolic_tangent_neuron_dec, hyperbolic_tangent_neuron_dec, linear_neuron_dec
    ),
    nepoch = 300, momentum = 0
)
```

## Exercise 5.
> Equip the back-propagation method with momentum, and perform comparative analysis of the improvement on the dataset from Ex. 3.


```r
rmse.ex5 <- average_rmse(
    times = 10, X = X, Y = Y, input_count = 2, neuron_counts = c(5, 5, 1),
    layers = list(hyperbolic_tangent_neuron, hyperbolic_tangent_neuron, linear_neuron),
    nepoch = 300, momentum = 0.001
)
```

## Comparison


```r
rmse.ex <- rbind(
    data.frame(
        rmse=rmse.ex3, iteration=1:300,
        network = "Constant eta (0.01) without momentum"
    ),
    data.frame(
        rmse=rmse.ex4, iteration=1:300,
        network = "Decreasing eta without momentum"
    ),
    data.frame(
        rmse=rmse.ex5, iteration=1:300,
        network = "Constant eta (0.01) with momentum (0.01)"
    )
)
```


```r
p <- ggplot(rmse.ex, aes(x = iteration, y = rmse, group = network, colour = network)) 
p <- p + theme_bw() + ggtitle("Average RMSE by iteration. Number of trials: 10")
p <- p + geom_line()
p <- p + theme(legend.position = 'bottom', legend.text=element_text(size=6))
p
```

![](report_files/figure-html/unnamed-chunk-2-1.png)<!-- -->



```r
remove.packages("ipiNeuralNetworksEx3")
```

```
## Removing package from '/home/zero323/R/x86_64-pc-linux-gnu-library/3.3'
## (as 'lib' is unspecified)
```
