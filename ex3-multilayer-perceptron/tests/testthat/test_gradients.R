library(testthat)

expect_equal(
    min(sigmoid_function(seq(-100, 100, 1))),
    0
)

expect_equal(
    max(sigmoid_function(seq(-100, 100, 1))),
    1
)

expect_equal(
    sigmoid_function(seq(-100, 100, 1))[101],
    0.5
)

expect_equal(
    which.max(sigmoid_gradient(sigmoid_function(seq(-100, 100, 1)))),
    101
)

expect_equal(
    max(sigmoid_gradient(sigmoid_function(seq(-100, 100, 1)))),
    0.25
)

expect_equal(
    sigmoid_gradient(sigmoid_function(seq(-100, 100, 1)))[c(1, 201)],
    c(0, 0)
)

