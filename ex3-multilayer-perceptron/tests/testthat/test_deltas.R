library(testthat)

expect_equal(
    {
        network <- create_network(1, c(2, 1), list(linear_neuron, linear_neuron))
        network$weights[[1]][] <- matrix(rep(1, 4), nrow = 2)
        network$weights[[2]][] <- matrix(rep(1, 1), nrow = 1)
        network <- feed_forward(network, c(1))
        network <- compute_deltas(network, 1)
        network$deltas
    },
    list(
        matrix(c(NA)),
        matrix(c(-4, -4), nrow = 2), # Since all weights are 1 we propagate -4 and linear gradient is 1
        matrix(c(-4)) # Number on output is -4
    )
    
)

expect_equal(
    {
        network <- create_network(2, c(3, 1), list(sigmoid_neuron, sigmoid_neuron))
        w1 <- matrix(c(1, 0, 0, 0, 1, 0, 0, 0, 1), nrow = 3)
        w2 <- t(matrix(rep(0.5, 4)))
        network$weights[[1]][] <- w1
        network$weights[[2]][] <- w2
        network <- feed_forward(network, c(0, 1))
        network <- compute_deltas(network, 0.3147324)
        network$deltas    
    },
    list(
        matrix(c(
            NA,
            NA
        )),
        matrix(c(       # d[3]  w[2]  gradient[o[2]]
            -0.04915298, # -0.5 * 0.5 * 0.04915298
            -0.06250000, # -0.5 * 0.5 * 0.06250000
            -0.04915298  # -0.5 * 0.5 * 0.04915298
        )),
        matrix(c(
            -0.5
        ))
    ),
    tolerance = 0.002
)
